import { Component, OnInit } from '@angular/core';
import { Image } from '../image';

@Component({
  selector: 'app-picture-gallery',
  templateUrl: './picture-gallery.component.html',
  styleUrls: ['./picture-gallery.component.scss']
})
export class PictureGalleryComponent implements OnInit {

  images: Image[];
  selected: number;
  constructor() { }

  ngOnInit() {
  	this.selected = 0;
  	this.images = [];
  	this.images.push(new Image('TensorFlow', 'https://www.tensorflow.org/images/tf_logo_social.png'));
  	this.images.push(new Image('TensorFlow Icon', 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Tensorflow_logo.svg/245px-Tensorflow_logo.svg.png'));
  	this.images.push(new Image('Keras', 'https://www.luisllamas.es/wp-content/uploads/2019/02/tensorflowkeras.png'));
  }
}
